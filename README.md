# CI/CD with Kubernetes
This lab will walk through an example pattern for implementing CI/CD on a Kubernetes Platform.  This repo will contain instructions for the hands on lab and we will talk about concepts as we go.

## Environment

Everyone has access to a single control plan/3 worker node TKG Kuberntes Cluster in AWS.  We will disribute kubeconfig files to grant access to the lab clusters.

What OS is everyone running locally?  If it makes sense you can use a linux jumpbox for the lab.  Make sure that you have the following tools running on the machine you will use to interact with the lab cluster.

[kubectl](https://kubernetes.io/docs/tasks/tools/)

[octant](https://github.com/vmware-tanzu/octant/releases/)

[kp](https://github.com/vmware-tanzu/kpack-cli/releases/)

[git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

[yq](https://github.com/mikefarah/yq/releases)

Download the Kubeconfig file to enable access to the cluster.

[kubeconfig](https://tanzu-e2e-install.s3.amazonaws.com/config)

You will be granted cluster admin access on your lab cluster.

## Connect to the Cluster

| kubectl context                   | Ingress Domain   |
| ----------------------------------|:----------------:|
| afc-training1-admin@afc-training1 | *.afc1.tsfrt.net | 
| afc-training2-admin@afc-training2 | *.afc2.tsfrt.net |
| afc-training3-admin@afc-training3 | *.afc3.tsfrt.net |
| afc-training4-admin@afc-training4 | *.afc4.tsfrt.net |
| afc-training5-admin@afc-training5 | *.afc5.tsfrt.net |

We will assign a cluster context to you based on the kubeconfig file that was downloaded.  Once you have downloaded it, copy it into your .kube folder or if you do not want to overwrite it, set the `KUBECONFIG=<path to config file>`.

Once we sort out who is using which context you will run 

```bash

kubectl config use-context <the assigned context>

```

Confirm that you have access to the cluster by running

```bash

kubectl cluster-info

```

Also, setup and run octant to confirm that you have access through Octant as well.

## Cluster Architecture

Each cluster has be setup with the following toolchain:

- Tanzu Build Service - Automated Container builds
- Elastic+fluentbit+kibana - log aggregation
- Prometheus+Grafana - metrics/observability
- Tanzu SQL PostgreSQL Operator - SQL DB
- Harbor Image Registry - Image Registry, Scanning, etc.
- SonarQube - SAST, Code Quality
- kubeapps - self service middleware 
- Contour - Ingress Controller
- Cert Manager - generation of TLS certs for ingress, etc.

We will touch on each of these technologies as we walk through the lab (some more than others).  These component have been pre-deployed to facilitate the lab.

Your cluster will be assigned a domain.  Lets confirm you have one and try out the following URLs (also validate the certificates resolve for the URLs):

https://kibana.your_domain

https://grafana.your_domain

https://kubeapps.your_domain

https://harbor.your_domain

https://sonar.your_domain

## GitLab

We will be using GitLab for version control and CI Orchestration in this lab.  You need to create a free account on GitLab.com, if you have not already done so.  You will want to fork the following projects into your own account to be used for this lab.

To fork the projects follow these instructions

[Forking in GitLab](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork)

### Spring Music

The Spring Music application is a simple CRUD application that was written in Java on the Spring Framework.  It interacts with a PostgreSQL database to store data and exposes a basic web interface.  The application is built and scanned using GitLab CI and it is containerized by Tanzu Build Service.  We will use this application to walk through what it looks like run continuous integration on an enterprise application.

https://gitlab.com/afc-training/spring-music

### Continuous Deployment

The continuous Deployment project is used to manage the state of the Spring Music application using a GitOps pattern.  It contains a yaml manifest that is templated using ytt to store the state of the Spring Music application across multiple environments.

https://gitlab.com/afc-training/continious-deployment

Once you have forked the projects, clone them to your local machine and open them within VS Code or your editor of choice.

You will want a local copy of:
- Training
- Spring Music
- Continuous Delivery

```bash

mkdir k8s-cicd
cd k8s-cicd

git clone https://gitlab.com/afc-training/training.git

git clone #your forked spring-music

git clone #your forked Continuous Deployment

```

## Hooking Up GitLab to your Cluster

The Entire GitLab Application can be deployed on Kubernetes, but for our purposes we will use the SaaS based instance of GitLab.  We will integrate this GitLab with our cluster and run a GitLab Runner in the cluster.  The GitLab Runner is where our build tasks are executed.  GitLab leverages the elastic nature of Kubernetes scaling so that when there are CI/CD jobs to run it will scale runner pods to perform the work and scale back down when idle.

Before proceeding, make sure that the gitlab service account is deployed on your cluster. The yaml deployment is located in the dpenencies folder of this repo.  There are also a couple helpful scripts in there that get referenced in the next section.

```bash
#in the training project
kubectl apply -f dependencies/gitlab.yaml

```

The following instructions will walk us through setting up the Runner in our forked Spring Music Project.

[Setup GitLab CI/CD](docs/gitlab_setup.md)

You should now have a GitLab Runner running in your cluster.  Now we are ready to discuss our Continuous Integration Pipeline.  

## Continuous Integration 

Lets create a merge request and walk through our sample CI Pipeline.

## Preparing For Continuous Deployment

We have walked throught the CI phase of our Pipeline and ended up with the Spring Music application published to a container registry.  Now we want to deploy and manage the life-cycle of that application across environments.  We want to use an approach to managing our application life-cycle that is similar to the reconciliation loop pattern we see in Kubernetes.  In order to achieve this, we will deploy an Open Source Operator called Kapp Controller into our cluster. 

## Deploying Kapp Contoller

Installing Kapp Controller is very straight forward.  

[Kapp Controller Install](https://github.com/vmware-tanzu/carvel-kapp-controller/blob/develop/docs/install.md)

Validate your deployment by check that app CRD is installed in your cluster

```bash

kubectl get crd apps.kappctrl.k14s.io 

```

Ok now that you have Kapp Controller installed you are ready to begin continuously deploying your applications and managing their configuration through GitOps

## Setup Continuous Deployment Project

[CD Setup Instructions](docs/cd.md)



